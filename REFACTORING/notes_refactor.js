REFACTOR

Characteristics of a Clean code: 

It should be elegant — Clean code should be pleasing to read. Reading it should make you smile the way a well-crafted music box or well-designed car would. 

Clean code is focused —Each function, each class, each module exposes a single-minded attitude that remains entirely undistracted, and unpolluted, by the surrounding details. 

Clean code is taken care of. Someone has taken the time to keep it simple and orderly. They have paid appropriate attention to details. They have cared. 

Runs all the tests 

Contains no duplication 

Minimize the number of entities such as classes, methods, functions, and the like. 


1) Able to explain the goal of refactoring

- reduce code duplication, make the code more Readable, so that if a next person has to work with your code, they will have to spend the least time possible trying to understand it
- make sure your code is as universal as possible, which means it is reusable
- follow the SINGLE RESPONSIBILITY PRINCIPLE, DRY (Dont repeat yourself) and KIS (Keep it simple) principles

2) Able to point out code smells

- when you notice weird code: code duplication, unnecessary nested for loop, unused variables, variables that are not meaningful, a method responsible for various things
- long methods (max 4 - 20 lines): if you see a method that is too long, try to reuse a part of it in another logic
- if it doesnt adhere to SRP 
- duplicate logic in the same class or the same logic used in two different classes
- long switch-case statements
- lazy classes (that hardly contain functions)
- monolytic: all the code is contained within one file -> this should be avoided

3) Able to explain the need for style guides and linters 

- styleguide: e.g. AirBnb, Google
- EsLint is used to make sure styleguides are being followed
- if more devs work on one project, it is worth/necessary to set one styleguide to which everyone will stick and write code the same way => this way, the code will be more readable and all this will help everyone to read the code of others faster, as it will be styled following the same rules
- using styleguides will help you to avoid doing mistakes (forgotten semicolons, left-out returns, too long lines of code)
- LINTERS: catches styling errors, strange code blocks and bugs
- noone can push such code until there are such errors in it
- this facilitates code review, as it has already gone through a test
- it is easier to see these errors already while coding and debug them then instead of when the app is already in production and you are unable to find the mistake
- also warns about given error handling and it is possible to configure it as needed
- in tslint.json we can configure the rules, for instance "no-trailing-whitespace": true

4) Able to recognize redundant code

- when you use the same code various times, but you do not put it into a separate function, or or you call the object properties many times, typed as user.id, user.name and do not use object destructuring instead: const { name, id } = user;

5) Able to reduce the complexity of a method

- reduce the cyclomatic complexity => use as less if statements (or control flow statements) as possible
- if used, make sure you use as less nested if statements as possible, too
- also, if you can use ternary operators instead of if-else, or use object destructuring, then do so, because it's cleaner and easier to read


MAGYARUL:

1) Able to explain the goal of refactoring

- csökkenteni a code duplicationt, olvashatóbbá tenni a kódot, hogyha más valaki megkapja, akkor minél kevesebbet kelljen az értelmezésével tölteni (hogy minél egyértelműbb legyen), törekedni arra, hogy minél ’univerzálisabb legyen’, tehát több esetre is felhasználható, követni a single responsibility elvét, felszámolni a nem használt kódot
 
DRY = don’t repeat yourself
KIS = keep it simple


2) Able to point out code smells

https://codeburst.io/write-clean-code-and-get-rid-of-code-smells-aea271f30318

- amikor látod, hogy valami nagyon nem stimmel: code duplication, fölösleges nested for loop, nem használt változók, a változók nem meaningfulok, egy method több dologért felelős
hosszú methodok (max 4-20sor): ha látod, hogy túl hosszú egy method, érdemes pár sort kiszervezni egy másik logikába

- ha nincs meg a single responsibility principle

- duplikált logika ugyanabban a classban vagy két különbözőben ugyanaz
- hosszú switch-case statement
- lazy classes (amik alig tartalmaznak funckiót)
- monolytic: az egész kód egy alkalmazásra vonatkozóan egy fájlban van megírva 🡪 elkerülendő

3) Able to explain the need for style guides and linters

- styleguide: pl. airbnb, google
- es lint az eszköze annak, hogy a styleguide-t betartassa

- ha többen dolgoztok egy projekten, akkor érdemes/muszáj megadni egy style guide-ot ami alapján mindenki dolgozik, hogy mindenki ugyanúgy írja meg a kódot => olvashatóbb lesz a kód (pl. az indentálások egy helyen vannak, és mindez megkönnyíti egymás kódjának a megértését mert vizuálisan rááll a szemed, hogy hogy működik a kód és nem kell időt tölteni a „stílus” bogarászásával) és szűri a hibalehetőségeket (lefelejtett pontosvesszők, kihagyott return-ök, túl hosszú kódsorok)

LINTER: elkapja a stilisztikai hibákat, a furcsa kódblokkokat és bugokat, nem tud úgy senki kódot felpusholni, hogy ilyen hibák vannak benne, ami megkönnyíti a review-t, mert már átment egy szűrőn
- könnyebb kódolás közben látni ezeket a hibákat és debuggolni, mint akkor, amikor már futna az alkalmazásod, de nem tudod mi benne a hiba
- jelzi a rosszul megírt error handlinget is és amúgy olyanra lehet konfigurálni, amilyenre akarod
- a tslint.json file-ban lehet beállítani a szabályokat, pl:
"no-trailing-whitespace": true,

4) Able to recognize redundant code

- mikor többször ugyanazt a kódsort használod, de nem szervezed ki egy külön függvénybe, vagy egy object propertyjeit sokszor hívod meg, és mindig kiírod, hogy user.id, user.name, ehelyett lehet destructuringot használni: const { name, id } = user;

5) Able to reduce the complexity of a method

- csökkenteni a cyclomatic complexity (cikk róla itt: 
  https://en.wikipedia.org/wiki/Cyclomatic_complexity), aminek az a lényege, hogy minél kevesebb if statement (vagyis control flow statement) legyen a kódban, és ha lehet, azok se legyenek egymásba ágyazva
- ide tartozik még, hogy amit meg lehet írni kevesebb sorban (pl. ternary operator if-else helyett, destucturinget használni objecteknél), azt írjuk meg kevesebb sorban, mert átláthatóbb és olvashatóbb

