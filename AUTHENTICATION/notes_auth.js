AUTHENTICATION 

https://www.youtube.com/watch?v=7nafaH9SddU&t=550s – szuper videó a JWT autentikációról, exp. date hozzáadással

TOKEN (access)
- lentebb kifejtve

Find user by token

- ha a token generálásnál beleraktad paraméternek a usernevet, akkor amikor kicsomagolja, és ellenőrzi, hogy a tokenben kódolt usernév egyezik-e azzal, amit mondjuk a body-ban szintén elküldtél, akkor duplán le lehet vele ellenőrzni, hogy te vagy-e aki hozzá akarsz férni az adataidhoz

- jwt.sign-nál a payload-ban megadhatjuk a usernek a nevét vagy id-ját. Miután visszajött a backendre a token, kicsomagoljuk és összehasonlítjuk az itt levő user nevét vagy id-ját az adatbázisban levő adattal

Header
- itt küldjük a tokeneket (authentication és authorization)

Status (401, 403)
401: unathorized (you have to login)
403: forbidden (not authorized, something went wrong)

Examples:
Usage of refresh tokens – ezt nem használjuk, úgyhogy még utána kell néznem -  https://solidgeargroup.com/refresh-token-with-jwt-authentication-node-js 

- miután lejárt az access token (ha van lejárati ideje), utána hívódik meg a refresh token, amely ezután már access tokenné válik

Add authorization header frontenden
<link> Nincs, mert nem jutottunk el idaig a projektben

Check authorization header
C:\Megalotis-Pounamu-Backend\src\middlewares\authenticator.js – a backend lecsekkolja a frontról a headerben érkező authTokent
jwt verify (checkToken függvény)

Check access token
C:\Megalotis-Pounamu-Backend\src\controllers\authenticationController.js – a 10.sorban ellenőrzi a google, hogy az access token ehhez a userhez tartozik-e

Send access token
C:\Megalotis-Pounamu-Frontend\src\app\services\auth\authorization.service.ts – a 28.sorban küldjük el a google-től kapott access tokent

Add expiry date to tokens
C:\Megalotis-Pounamu-Backend\src\controllers\authenticationController.js – Itt a 21. sorban lehet adni a sign-nak egy objectben egy olyan paramétert, hogy {expiresIn: 30}, és ez megmondja, meddig érvényes (nem tudom, ha nem adsz meg neki semmit, akkor mennyi a default). Ha számot adsz meg, akkor sec-ben fogja mérni, ha stringként „60”, akkor az milisecundum, de ha azt mondod, hogy „2 days” vagy „7h”, akkor ezt is elfogadja
https://github.com/auth0/node-jsonwebtoken itt leírják, hogy pontosan hogy lehet megadni az expiry időt


Expected skills (3/4)

Able to explain the aim of authentication

-az autentikáció lényege, hogy amikor belépsz a login adataiddal egy weboldalra, leellenőrzik, hogy tényleg az vagy-e, akinek mondod magad, hogy csak te férhess hozzá az adataidhoz


Able to show what is a token

<link> add link here from our repo

- a token egy hosszú, betűkből és számokból álló szám/betűsor, ami kül. algoritmusos kódolással encoded  (lehet bele tenni adatokat, pl user name, password), és amit csak ezzel a kódolással és egy kulccsal lehet ’kicsomagolni’
C:\Megalotis-Pounamu-Frontend\src\app\services\auth\authorization.service.ts => ha itt a 27. sor alatt kilogolod az idTokent, az a google által küldött tokent, amit elküldünk a backendnek, hogy még egy lépcsőben kérdezze meg, hogy ez a token tényleg ehhez a userhez tartozik-e => authentication
ugyanebben a fileban ha a 29.sor alatt kilogolod, hogy user.authToken, az már az authorization token, amit a jwt csinál, miután a google leokézta, hogy a user tényleg az, akinek mondja magát. innentől kezdve ezt az authTokent küldözgeti a backend és a frontend egymásnak, és a back folyamatosan ellenőrzi, hogy a token ahhoz a userhez tartozik-e => authorization


Able to show how it is used to protect a resource

<link> add link from whoever did the resources

C:\Megalotis-Pounamu-Backend\src\controllers\authenticationController.js – itt a 10-es sorban csekkolja a google (ez a saját függvényük), hogy egyezik-e a user és a hozzá tartozó token, és ha igen, akkor beenged (és csinál a jwt egy authTokent a 21.sorban), ha nem, akkor pedig failed authentication errort fog dobni => ez az authentication
C:\Megalotis-Pounamu-Backend\src\middlewares\authenticator.js – a headerben beérkező tokent itt csekkolja le a jwt, és csak akkor engedi tovább, ha a 8.sorban a jwt. verify nem dob errort => ez az authorization 


Able to demonstrate an authentication error

<link> add link here

C:\Megalotis-Pounamu-Frontend\src\app\services\auth\authorization.service.ts => ha itt a 27. sor alatt kilogolod az idTokent és mondjuk postmanben lekéred a GET /auth endpointunkat úgy, hogy mondjuk kicserélsz a tokenben egy betűt (vagy be sem írsz tokent), akkor ezt fogja dobni:
