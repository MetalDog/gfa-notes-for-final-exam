DEVELOPMENT OPERATIONS

Git: A Git egy ingyenes, open source verziókontroll rendszer, ami lehetővé teszi, hogy többen dolgozzunk egy projekten remote módon, és mindig hozzá tudjuk férni a legfrissebb működő kódhoz
Git a verziókövető rendszer, a github pedig egy hosting service a git repositoryknak

Hosting:
A hosting azt jelenti, hogy egy weboldalnak biztosítanak egy szervert, amin futhat, és amin elérik a felhasználók. ez általában úgy működik, hogy van pl. az amazonnak egy hatalmas szerver parkja, és abban lényegében különböző szerverek vannak (computerek), és akkor ott kapsz egyet, amin majd futni fog az alkalmazásod. ennek van egy IP címe, és itt fogják elérni a felhasználók. Egy computeren több port van, tehát több weboldal is futhat ugyanazon az IP címen, csak a gép különböző portjain
https://www.techopedia.com/definition/29023/web-hosting	

Deploy:
A kód releaselése a szerverre a felhasználók számára.
A github kód „felrakása” a szerverre (pl: heroku, aws)


CI

Bash: egy terminál típus. A Unix operációs rendszernél használják
(A command language interpreter, a free Unix shell. A shell is a user interface for access to an operating system's services.) Bash is basically a command processor that typically runs in a text window, allowing the user to type commands that cause actions. It can read commands from a file, called a script

Build: A build lehet egy program verziója (buildje), ami általában még fejlesztés alatt áll. Azt is jelenti, hogy ’buildel’, tehát összerakja egy program kódjait (mint amikor ng servelünk, és kiírja, hogy building).

Environment variables: https://medium.com/chingu/an-introduction-to-environment-variables-and-how-to-use-them-f602f66d15fa olyan változók, amiknek az értékét a programon kívül adod meg (pl a connection stringünk az adatbázisunkhoz a .env fájlban). Azért jók, mert ha lecseréled mondjuk az adatbázist, akkor nem kell a source kódhoz hozzányúlni, és releaselni egy új verziót, hanem elég, ha csak a .env fájlban lecseréled a változó értékét. 

Examples:

Environment based API base url: 

- frontend environment file-jában megadni, hogy productionnél milyen változókat használjon (vagyis különböző lesz a URL változó értéke)
- és developmentnél: url = /api/  (azért nem kell localhost 3000, mert a proxy configban átirányítódik a localhost 3000-re)

- produkcional: url = https://gfreddit.herokuapp.com/

Create secrets or database config based on environment

- az adatbázisodhoz a connection string a process.env.MONGO_URI legyen, és csak megváltoztatod a .env fájlban, ha le akarod cserélni

Using different services in test env: httpclientmodule vs httpclienttestingmodule
https://www.thoughtworks.com/continuous-integration – cikk a CI-ről és a CD-ról
https://www.martinfowler.com/articles/continuousIntegration.html – CI
https://continuousdelivery.com/2010/08/continuous-delivery-vs-continuous-deployment/ - continuous delivery vs. continuous deployment

Expected skills (4/5)

1) Able to recognize and resolve merge conflicts

- merge conflict akkor keletkezik amikor mergelsz egy barnchet egy másikba, és a rendszer szól, hogy eltér egymástól a két kód, és ha egy helyen (pl ugyanabban a fájlban) két verzió van a kódból (egy, ami a branchen van ahova mergelnél, a másik pedig ami a te verziód), akkor szól, hogy mondd meg melyiket tartsa meg (egyik, másik mindkettő), és ilyenkor össze kell fésülnöd a két kódot, hogy összeférhető legyen a két verzió miután átnézed a kódot és feloldod a conflictokat, git addolod a változtatásokat, és commitolsz, így becsekkolod az összefésült kódot és véglegesíted a merge-öt


2) Able to explain workflow details on collaborating with version control

- általában van egy master és egy dev branch, a master tartalmazza a production kódot, a dev pedig a development kódot
a workflow menete hogy a különböző feature-ökre feature brancheket hozunk létre, amik a devből ágaznak ki, és azokon fejlesztünk. amikor befejeztünk egy feature-t (git add., git commit, git push), pull requestet kérünk a dev-be, és miután elfogadták, bemergeljük a branchünket (majd kitöröljük)
idő közben pedig a deven letesztelt és approvált feature-öket be lehet mergelni a master (production) branchbe, ahonnan pedig deploye-oljuk


3) Able to explain the flow of releasing code to production servers

- Create app on heroku 🡪 git remote -a app neve (összekötjük a master branchünket a herokuval) 🡪 debuggoljuk az esetleges hibákat (majd git add, git commit) 🡪 git push heroku master (felküldjük a legfrissebb kódot a heroku szerverre)

- continuos integration: a Circle CI lecsekkolja a felpusholt kódunkat, és szól, ha valami nem működik/nem jól van megírva. nálunk Gábor nem nézi meg addig a requesteket ameddig nem mennek át CI-on és csak utána approválja mergre.

- a continuous integrationt be lehet úgy állítani, hogy ha átmegy a kód, akkor automatikusan integrálja mondjuk a dev-be 

- continuos delivery: a continuous delivery azt jelenti, hogy nem releaselsz minden új feature-t automatikusan a felhasználóknak, hanem van egy hard stop mielőtt ez megtörténne, és kell egy business reason ami igazolja a release-t, és csak ez után történhet meg a deployment. tehát a release nem automatikus, ugyanakkor a fejlesztőknek mindig release kész kódot kell írniuk (mondjuk ez amúgy is elég egyértelműen így kellene, hogy legyen…)

- continuoUs deployment: a continuous deployment azt jelenti, hogy azokat a featureöket, amik átmentek a continuous integrationön, és le vannak tesztelve, automatikusan deployolják a szerverre, és a felhasználók azonnal hozzá is férhetnek


4) Able to show how the application is configured for different environments

- itt meg tudjuk mutatni, hogy két environmentünk van, a production (ami a master branchünk) és a development (ami a dev branchünk), a devben fejlesztünk, a mastert pedig arra használjuk, hogy deployeoljuk a weboldalunkat

- megmutathatnánk azt is, hogy vannak environment variable-jeink, pl a process.env.CONNECTION_STRING, ami két különböző adatbázishoz tartalmaz connection stringet (tehát tesztingre van egy külön environmentünk)

- különböző environment (szerintem) az is, amikor npm run start:server-t indítunk, mert akkor úgy indul el a frontend,  hogy a mock adatbázist használja, tehát van egy külön environmentünk hogy ne az éles adatbázisba fejlesszünk közvetlenül 

Mindegyik environmenthez tartozik egy branch.
-    development: feature branchek. Az összes kész feature-t merge-eljük a dev-be
-    staging branch-ben letesztelik az egészet
-    production: master-be merge-elik az egészet, ha minden működik és készen áll a release-re

CI-os environment variable-k: testhez és developmenthez külön-külön .env fájlok vannak: más a CONNECTION.STRING a tesztnél és a developmentnél (CircleCI oldalán a beállításokban)


5) Able to build the app from the command line interface

- szerintem itt arra gondolnak, hogy el tudod indítani a frontendet ng serve-vel (vagy npm run start:server vagy npm run start:proxy), a backendet pedig npm run-nal (vagy nodemon index.js)
 

ANGOLUL:
 
CONTINUOUS INTEGRATION (CI)
how does the code on github go live to the server

Without CI:

1. CODE:
     - each feature has its own branch
     - coding the feature
     - testing the feature

2. BUILDING:
     - check the styleguide: code is readable, etc.
     - check if all tests pass
     - build the application: a program futtatása. (compiling, pl. ts node, ng serve parancsok futtatása), honlapon funkció ellenőrzése (betöltés, kattintás)

3. INTEGRATION:
     - pull request
     - code review
     - merge the feature branch

4. RELEASE:
     - versioning: there are new features implemented. Informing users about them by releasing new versions

5. DEPLOY:
     - log into the server (pl. heroku)
     - stop the running application
     - pull the latest release
     - start the application

Without CI, the whole process is slow --> automation is faster

With CI:

CI tools: jenkins, circleCI, travis CI

These tasks below become automated, the developer doesn't have to do these steps manually:

- check the styleguide
- check if all tests pass
- build the application

We can adjust CI to check by every pull request or by every commit.

CONTINUOUS DELIVERY (CD)
The five steps in the deploy part become automated. It's faster.
Once the developer clicks on merge, the new code is out and live on the server (automatic deployment). However, we can stop the process if sg goes wrong.

APPLICATION ENVIRONMENTS
- development: writing code and testing it
- staging: not only the developer tests the code but there is a separate team for testing. (éles, valódi adatokat nem használunk fel)
- production: amit a user lát (a development mindig előrehaladottabb)

Zero downtime deployment
