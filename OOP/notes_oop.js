Object Oriented programming

- a programming paradigm centered around objects, rather than functions


4 pillars of OOP


ENCAPSULATION

Benefits: 

- reduces complexity
- increases reusability

Example with no encapsulation:
(takes properties as function parameters)

let baseSalary = 30000;
let overtime = 10;
let rate = 20;

function getWage(baseSalary, overtime, rate) {
  return baseSalary + (overtime * rate);
}

Example with OOP solution:
-no parameters in function needed, or fewer parameters needed than in the procedural method above 
-all properties and the functions are highly related as they are part of one unit

let employee = {
  baseSalary: 30000,
  overtime: 10;
  rate: 20,
  getWage: function() {
    return this.baseSalary + (this.overtime * this.rate);
  }
};
employee.getWage();


ABSTRACTION

Benefits:

- hides the details and complexity and only shows the essentials
- simpler interface
- reduce the impact of change


INHERITANCE

- eliminates the redundant code

POLYMORPHISM

- technique that allows us to refactor ugly if/else or switch statements









